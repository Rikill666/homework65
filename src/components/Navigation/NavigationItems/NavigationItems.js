import React from 'react';
import {Nav} from "reactstrap";
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => {
    return (
        <Nav >
            <NavigationItem to="/" exact>Home</NavigationItem>
            <NavigationItem to="/pages/contacts" exact>Contacts</NavigationItem>
            <NavigationItem to="/pages/about" exact>About</NavigationItem>
            <NavigationItem to="/pages/divisions" exact>Divisions</NavigationItem>
            <NavigationItem to="/pages/admin" exact>Admin</NavigationItem>
        </Nav>
    );
};

export default NavigationItems;