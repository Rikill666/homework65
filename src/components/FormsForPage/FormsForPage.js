import React from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";


const FormsForPage = (props) => {
    return (
        <Form onSubmit={props.pageHandler}>
            <FormGroup>
                <Label for="exampleSelect">Select page</Label>
                <Input
                    type="select"
                    name="category"
                    id="exampleSelect"
                    onChange={props.categoryValueChanged}
                    value={props.category}
                >
                    {props.pages.map((c) => (
                        <option key={c.id} value={c.id}>{c.title}</option>
                    ))}
                </Input>
            </FormGroup>
            <FormGroup>
                <Label for="exampleTitle">Title</Label>
                <Input type="text"
                       name="title"
                       id="exampleTitle"
                       onChange={props.valueChanged}
                       value={props.title}
                />
            </FormGroup>
            <FormGroup>
                <Label for="exampleDescription">Content</Label>
                <Input type="textarea"
                       name="description"
                       id="exampleDescription"
                       onChange={props.valueChanged}
                       value={props.description}
                />
            </FormGroup>
            <Button>Save</Button>
        </Form>
    );
};

export default FormsForPage;