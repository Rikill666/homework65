import React, {Component} from 'react';
import FormsForPage from "../../components/FormsForPage/FormsForPage";
import axiosOrder from "../../axiosOrders";

class Admin extends Component {
    state = {
        pages: [],
        title: "",
        description: "",
        category:""
    };

    valueChanged = event => this.setState({[event.target.name]: event.target.value});
    categoryValueChanged = event => {
        let id = event.target.value;
        const page = this.state.pages.find(t => t.id === id);
        this.setState({category: event.target.value, title:page.title, description:page.description});
    };

    pageHandler = async event => {
        event.preventDefault();
        const page = {
            title: this.state.title,
            description: this.state.description,
        };
        await axiosOrder.put('/pages/' + this.state.category + '.json', page);
        this.props.history.replace('/pages/' + this.state.category);
    };

    async componentDidMount() {
        const response = await axiosOrder.get("/pages.json");
        const pages = Object.keys(response.data).map(id => {
            return {...response.data[id], id};
        });
        this.setState({pages, category:pages[0].id, title:pages[0].title, description:pages[0].description});
    };

    render() {
        return (
            <div>
                <h1>Edit pages</h1>
                <FormsForPage
                    pages={this.state.pages}
                    valueChanged={(e) => this.valueChanged(e)}
                    pageHandler={(e) => this.pageHandler(e)}
                    title={this.state.title}
                    description={this.state.description}
                    categoryValueChanged={(e) => this.categoryValueChanged(e)}
                />
            </div>
        );
    }
}

export default Admin;