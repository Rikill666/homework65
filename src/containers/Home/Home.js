import React, {Component} from 'react';
import axiosOrder from "../../axiosOrders";
import Spinner from "../../components/UI/Spinner/Spinner";

class Home extends Component {
    state = {
        pages: {},
        loading: true
    };
    requestData = async() => {
        try{
            let url = '/pages/';
            if(this.props.match.path === "/"){
                url += "home";
            }
            else{
                const path = this.props.location.pathname.split("/");
                url += path[path.length - 1];
            }
            const response = await axiosOrder.get(url +".json");
            const pages = response.data;
            this.setState({pages});
        }
        finally {
            this.setState({loading:false});
        }
    };
    async componentDidMount() {
        this.requestData();
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.location.pathname !== this.props.location.pathname){
            return this.requestData();
        }
    }

    render() {
        let pages = <Spinner/>;
        if(!this.state.loading){
            pages = <div><h1>{this.state.pages.title}</h1><p>{this.state.pages.description}</p></div>
        }
        return pages;
    }
}

export default Home;