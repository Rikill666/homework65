import React from 'react';

import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Home from "./containers/Home/Home";
import Admin from "./containers/Admin/Admin";

const App = () => (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/pages/admin" exact component={Admin}/>
          <Route path="/pages" component={Home}/>
          <Route render={() => <h1>Not Found</h1>}/>
        </Switch>
      </Layout>
    </BrowserRouter>
);

export default App;
